#include <SoftwareSerial.h>

/*
 Copyright (C) 2014 Jari Suominen
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
  
int subnets[] = {7,4,3,2};
int activeSubnet = -1;

#define MAX_SERIAL_WAIT_MILLIS 100

  /*
  Serial API documentation !!!
   
   NOTE: All bytes (but the command byte) should always have 0 as the first bit followed by 7-bit value
   (range will then be from 0-127). For values split to two bytes, two similarly composed 7-bit
   values are to be used to represent most and least significant bytes.
   */

  /*                                      byte 0 | byte 1  | byte 2        | byte 3        | byte 4   */
#define SET_FREQUENCY  0xF0             // 240 | address | frequency LSB | frequency MSB
#define SET_VELOCITY   0xF1             // 241 | address | volume
#define SET_MODE       0xF2             // Not used (YET)
#define ADDRESS_ASSIGN 0xF3             // 243 | address
#define SET_FREQUENCY_AND_VELOCITY 0xF4 // 244 | address | frequency LSB | frequency MSB | volume
#define SET_WAVEFORM        0xF5        // 245 | address | waveform (check values below)
#define SET_LED             0xF6        // 246 | address | LED # (0/1) | brightness (0 = off, other values = on)
#define SET_FADE_SPEED      0xF7        // 247 | address | speed LSB | speed MSB (speed = volume units / second)
#define SET_PORTAMENTO      0xF8        // 248 | address | speed LSB | speed MSB (speed = frequency units / second)
#define SET_LP_FILTER       0xF9        // 249 | address | on (0 = off, other values = on)
#define RESET_NEXT          0xFA        // 250 | address | include address assignment (0=no, other values = yes)

#define WAVEFORM_SQUARE   0x00          // 1
#define WAVEFORM_SAW      0x01          // 2
#define WAVEFORM_TRIANGLE 0x02          // 3
#define WAVEFORM_NOISE    0x03          // 4
#define WAVEFORM_SINE     0x04          // 5
#define WAVEFORM_COLORED_NOISE 0x05     // 6 // NOT IMPLEMENTED !!!

void setup() {
  Serial.begin(115200);
  muteAll();  
}

void loop() {
  
  if (Serial.available() < 1) return;
  
  byte command = Serial.read();
  if ((command & B10000000) != 0x80) { // == data byte
    return;
  } 

  if (!byteReady()) return;
  if ((Serial.peek() >> 7) > 0) return;
  byte addr = Serial.read();

  if (command == ADDRESS_ASSIGN) {
    //digitalWrite(DEBUG_LED,!digitalRead(DEBUG_LED));
    /*
      Sending the assigment message to the subnets 
     Each distributor can command 127 nodes, and each subnet
     can have max 31 nodes in them. NOTE: there might be unused
     node addresses, depending of the net configuration.
     */
     
    for (int i = 0; i < 4; i++) {
      activateSubnet(i);
      Serial.write((byte)0xF3);
      Serial.write((byte)0);
      delay(1); // checkaa tän tarpeellisuus!
    }
    return; 
  }

  /* 
   Other commands will be simply forwarded to the correct subnet.
   */
  int sn = addr / 32;
  
  activateSubnet(sn);
  Serial.write(command);
  Serial.write((byte)(addr - sn * 32));
   
  while (true) {
    if (!byteReady()) return;
    if ((Serial.peek() & B10000000) != 0x80) { // == data byte
      byte r = Serial.read();
      Serial.write(r);
    } 
    else {
      return;  
    }
  }
}

/*
  Reads 2 7-bit values from Serial and combines them to one (unsigned) int.
 -1 is returned if value could not be read.
 */
int read2bitValue() {
  if (!byteReady()) return -1;
  if ((Serial.peek() >> 7) > 0) return -1; 
  byte lsb = Serial.read();
  if (!byteReady()) return -1;
  if ((Serial.peek() >> 7) > 0) return -1; 
  byte msb = Serial.read();
  int value = msb * 128 + lsb;
  return value;
}

void send2bitValue(int i) {
  if (i > 16383 || i < 0) return;
  byte lsb = i & 0x7F;
  byte msb = i >> 7;
  Serial.write(lsb);
  Serial.write(msb);
}

/*
  false = timeout occurred, nothing to be read
 true  = byt can be read
 */
inline boolean byteReady() {
  long startTime = millis();
  while (Serial.available() < 1) {
    if (millis()-startTime > MAX_SERIAL_WAIT_MILLIS) {
      return false; 
    } 
  }
  return true;
}

void muteAll() {
  for (int i = 0; i < 4; i++) {
      pinMode(subnets[i],OUTPUT);
      digitalWrite(subnets[i],HIGH);
      activeSubnet = -1;
  } 
}

void activateSubnet(int n) {
  if (activeSubnet > -1) {
    if (activeSubnet == n) return;
    delay(1); // tsekkaa tän tarpeellisuus
    pinMode(subnets[activeSubnet],OUTPUT);
    digitalWrite(subnets[activeSubnet],HIGH);    
  }
  pinMode(subnets[n],INPUT);
  digitalWrite(subnets[n],LOW);
  activeSubnet = n;
}


